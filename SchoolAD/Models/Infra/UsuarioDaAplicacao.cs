﻿using Microsoft.AspNetCore.Identity;

namespace SchoolAD.Models.Infra
{
    public class UsuarioDaAplicacao : IdentityUser
    {
        public string SobreNome { get; set; }
        public string Nome { get; set; }
    }
}
