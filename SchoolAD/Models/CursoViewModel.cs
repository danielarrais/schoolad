﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolAD.Models
{
    public class CursoViewModel
    {
        public string Nome { get; set; }
        public long? DepartamentoID { get; set; }
    }
}
